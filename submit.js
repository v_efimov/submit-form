"use strict";

function submitForm() {
    const form = document.querySelector('#myForm');

    form.addEventListener('submit', (e) => {
        e.preventDefault();

        const formData = new FormData(form);
        formData.append('newField', 'customValue')
        formData.delete('hiddenInput');

        const xhr = new XMLHttpRequest();
        xhr.open('POST', '/posturl/');
        xhr.send(formData);

        const span = document.createElement('span');
        span.id = 'submitted-form';
        span.classList.add('label', 'long');
        form.appendChild(span);

        xhr.onload = function () {
            if (xhr.status !== 200) {
                span.innerHTML = `Форма не отправлена (Error ${xhr.status} : ${xhr.statusText})`;
                span.classList.add('unsuccessful');
            } else {
                span.innerHTML = 'Форма отправлена';
                span.classList.add('successful');
            }
        }
    })
};

submitForm();

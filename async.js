const RESULT = {
	resolve: 'Promise fulfilled',
	reject: 'reject rejected',
};

const callPromise = () => new Promise((resolve, reject) => {
	const success = (Math.floor(Math.random() * 200) + 1 ) > 100;

	setTimeout(() => {
    	if (success) {
      		resolve(RESULT.resolve);
    	} else {
     		reject(new Error(RESULT.reject));
    	}
  	}, 900);
});

const task1 = () => {
	callPromise()
		.then(response => console.log('TASK1', response))
		.catch(error => alert('TASK1_Произошла ошибка'));
}
task1();

const task2 = async () => {
	try {
		const response = await callPromise();
		console.log('TASK2', response);
	}
	catch (error) {
		alert('TASK2_Произошла ошибка');
	}
}
task2();